# DataAnalyzer

[![N|Solid](https://southsystem.files.wordpress.com/2017/06/cropped-logo-south-system.png)](https://www.pucrs.br/wp-content/themes/pucrs-responsivo/hotsites/tecnopuc-experience/img/logo-southsystem.png)

[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://travis-ci.org/joemccann/dillinger)

Sistema Responsável por receber, analizar e gerar estatísticas através de arquivos brutos, tornando assim, suas informações refinadas utilizando um processo primitivo de ETL.

  - Automatização de processo e validação de arquivo
  
# New Features!

  - Quantidade de clientes no arquivo de entrada
  - Quantidade de vendedor no arquivo de entrada
  - ID da venda mais cara
  - O pior vendedor
  - O sistema deve estar funcionando o tempo todo (batch e real time).
  - Todos os arquivos novos devem estar disponível, tudo deve ser executado
  - flexibilidade em alteração do formato de entrada e saída dos arquivos.
  - Diretório para entrada e saída parametrizáveis
  - Swagger dos serviços
  - Serviços para consumo dos resultados disponíveis
  - visualização dos resultados através de arquivo e também do endpoint: http://localhost:8080/report/dataView
  - Teste e validação das linhas dos arquivos para verificar algum problema, gerando um log de quantas linhas fora de formatação e em quais arquivos e numero da linha
  

### Tech

Dillinger uses a number of open source projects to work properly:

* [Java 8] - Versão do java para compilação do sistema!
* [Spring Framework] Utilização de alguns dos componentes spring para agilidade no desenvolvimento
* [Spring Boot] - Para otimização do trabalho e foco na regra de negócio
* [Tomcat Embeded] - Servidor web embarcado para fácil build e execulção do projeto.
* [html] - html simples para exibição dos resultados na página web com auto-refresh
* [maven] - para gestão de dependências
* [Swagger] - Para documentação dos serviços criados
* [Factory e MVC] - Para generalização e centralização dos códigos comuns assim como separação de regras de negócio, modelos, serviços, controladores e ultilitários
* [Breakdance](https://breakdance.github.io/breakdance/) - HTML to Markdown converter
* [GIT] - Para gestão dos fontes do projeto

### Installation

Clonar o projeto de [Clone](https://gitlab.com/brurei10/dataanalizer.git).

Incorporar o projeto em seu IDE de preferencia (Eclipse, Netbeans, Intellj)

Executar os comandos do Maven:
- Maven Clean
- Maven Install
- E, caso não possua o plugin do spring boot, favor baixar e instalar;
- Após este passo, executar spring boot Aplication:
- Caso seja eclipse: Botão direito no projeto > Run as > Spring boot app.
- Aguardar o projeto subir. No console começará a aparecer dados de logo como os exemplos abaixo:
Linha nos padrões do arquivo
3
Linha nos padrões do arquivo
3
Linha nos padrões do arquivo
3
Linha nos padrões do arquivo
Total de linhas lidas: 68

Locais com defeitos:  Nome Arquivo: archive_test - Copia - Copia - Copia (2).dat
Linha defeituosa 7
 Nome Arquivo: archive_test - Copia - Copia - Copia - Copia.dat
Linha defeituosa 8
 Nome Arquivo: archive_test - Copia - Copia - Copia.dat
Linha defeituosa 9
 Nome Arquivo: archive_test - Copia - Copia.dat
Linha defeituosa 10

Total de linhas com defeito: 4
Total de arquivos lidos: 12

- Caso os diretórios do sistema nao estejam criados, o sistema fará isso pra você, criando-os no seguinte caminho: user.dir\data 
- O caminho de criação também é mostrado no console
- No meu caso criou no caminho : C:\Users\bruno.rafael\eclipse-workspace\data-analizer\data\

-Feito isso, basta colar os arquivos .dat no diretório seudiretoriocriado\data\in
Caso existam outros tipos de arquivo, eles serão desconsiderados, sendo validados pelo sistema apenas os .dat
- Para verificar o arquivo criado, basta acessar a pasta data\out validando assim o relatório gerado.
- Caso queira ver o resultado consultando um endpoint, basta acessar: http://localhost:8080/report/dataView.
- Tanto através do browser quando atraves de arquivos, são atualizados automaticamente sem necessidade de reinício da aplicação, bastando inserir novos arquivos na pasta que o resultado no serviço e no arquivo de saída serão atualizados automaticamente.

- CASO NÃO POSSUAM UM ARQUIVO MODELO, O PROJETO CONTÉM UM PARA A STEP DE TEST NO DIRETÓRIO /dataanalizer/src/test/java/com/br/south/bruno/data/test/archive_test.dat
- CASO O TIPO OU A ESTRUTURA INTERNA ESTEJAM DIVERGENTES DO ESPERADO, O SISTEMA EFETUA A VALIDAÇÃO E INFORMA A QUANTIDADE DE ERROS E O LOCAL.

- Obrigado e bons testes!